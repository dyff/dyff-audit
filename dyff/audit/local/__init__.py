# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

from .platform import DyffLocalPlatform

__all__ = [
    "DyffLocalPlatform",
]
