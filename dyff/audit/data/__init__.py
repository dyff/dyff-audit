# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0
"""Tools for working with input and output data for systems being audited."""

from . import text

__all__ = [
    "text",
]
